package ca.cmpt213.as3.UI;
import java.util.Scanner;
import ca.cmpt213.as3.Model.*;

/**
 * the Play class show the game play
 * user need to input a character and also a number
 * if user input wrong like reverse order input or input more than the program allowed, user need to input again
 * display both cheat matrix and also game matrix
 */
public class Play {


    private String letter;
    private int number;
    private int first_input = 0;
    private int second_input = 1;
    private int J_character_in_ascii_table = 74;
    private int A_character_in_ascii_table = 65;

    private Field hidden = new Field();
    private Field board = new Field("~");
    private Fortress playerFortress = new Fortress();
    private Tank[] tankInput;

    public void play_game(int numberOfTank, boolean cheat) {
        hidden.deployTank(numberOfTank);
        if (cheat == true){
            System.out.println(hidden.toStringTank());
        }
        tankInput = new Tank[numberOfTank];
        for (int i = 0; i < numberOfTank; i++){
            tankInput[i] = new Tank();
        }
        display(board);

        Scanner scan  = new Scanner(System.in);
        boolean checkingInput;

        while (playerFortress.getHealth() != 0) {
            System.out.println("Fortress Structure Left: " + playerFortress.getHealth());
            System.out.println("Enter your move: ");
            String user_input = scan.nextLine();
            checkingInput = checkInputCondition(user_input);
            if (checkingInput == false) {
                user_input = scan.nextLine();
            }
            else {
                letter = getLetter(user_input);
                number = getNumber(user_input);
                number = number - 1;
                String holdUserInput = user_input.toUpperCase();
                char character1 = holdUserInput.charAt(first_input);
                int ascii1 = (int) character1;
                ascii1 = ascii1 - A_character_in_ascii_table;
                int valueInTheCell = hidden.checkHit(ascii1, number);
                if (valueInTheCell == -1 || valueInTheCell > 96){
                    System.out.println("Miss. ");
                    int totalDamageFromTank = 0;
                    for (int i = 0; i < numberOfTank; i++){
                        totalDamageFromTank = totalDamageFromTank + tankInput[i].getDamage();
                        System.out.println("Alive tank #" + (i + 1) + " of shoot you for " + tankInput[i].getDamage() + "!");
                    }
                    playerFortress.gotHit(totalDamageFromTank);
                    board.setPlayerFieldMiss(ascii1, number);
                }
                else {
                    System.out.println("HIT!");
                    tankInput[valueInTheCell].tank_got_hit();
                    board.setPlayerFieldHit(ascii1, number);
                    hidden.setTankField(ascii1, number);
//                    for (int i = 0; i < numberOfTank; i++){
//                        System.out.println("Alive tank #" + (i+1) + " of shoot you for " + tankInput[i].getDamage() + "!");
//                    }
                }
                display(board);
                int totalTankHealth = 0;
                for (int i = 0; i < numberOfTank; i++){
                    totalTankHealth = totalTankHealth + tankInput[i].getHealth();
                }
                if (totalTankHealth == 0){
                    break;
                }
            }
        }
        if (playerFortress.getHealth() != 0){
            System.out.println("Congratulations! You won");
            System.out.println("Game Board: ");
            System.out.println(hidden.toStringTank());

        }
        else{
            System.out.println("I'm sorry, your fortress has been smashed!");
            System.out.println("Game Board: ");
            System.out.println(hidden.toStringTank());
        }
    }

    public String getLetter(String user_input){
        return user_input.substring(first_input,1);
    }
    public int getNumber (String user_input){
        return Integer.valueOf(user_input.substring(second_input));
    }

    public boolean checkMe(String s) {
        boolean amIValid = false;
        try {
            Integer.parseInt(s);
            // a valid integer!
            amIValid = true;
        } catch (NumberFormatException e) {
            //sorry, not an integer
            // we just move on
        }
        return amIValid;
    }

    public boolean checkInputCondition(String user_input) {
        String temp = user_input.substring(1);
        if (user_input.length() < 1 || user_input.length() > 2){
            if (temp.compareTo("10") != 0){
                System.out.println("Invalid target. Please enter a coordinate such as D10.");
                return false;
            }
        }
        String holdUserInput = user_input.toUpperCase();
        char character1 = holdUserInput.charAt(first_input);
        String character2 = user_input.substring(second_input);
        boolean checkingSecondInputIsInteger = checkMe(character2);
        int ascii1 = (int) character1;
        if (ascii1 > J_character_in_ascii_table ||
                ascii1 < A_character_in_ascii_table ||
                checkingSecondInputIsInteger == false) {
            System.out.println("Invalid target. Please enter a coordinate such as D10.");
            return false;
        }
        return true;
    }

    public void display(Field board){
        System.out.println("Game Board: ");
        System.out.println(board.toStringUSer());
    }
}
