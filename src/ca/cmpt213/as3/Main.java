package ca.cmpt213.as3;
import ca.cmpt213.as3.UI.Play;

/**
 * the main class receive the input from user which is number of tank
 * user also can choose to be a cheater
 * if user are not input any number, the default will be 5 tanks
 */
public class Main {

    public static void main(String[] args) {
        try {


            System.out.println("Let's play");
            // checking user input
            int numOfTAnk = 5;
            boolean cheatChecker = false;
            String cheat = "--cheat";

            Play game = new Play();
            if (args.length == 0) {
                System.out.println("Tank default " + numOfTAnk);
            } else if (args.length == 1) {
                numOfTAnk = Integer.parseInt(args[0]);
                System.out.println(numOfTAnk + " Tank in a field");
            } else if (args.length == 2 && cheat.equals(args[1])) {
                numOfTAnk = Integer.parseInt(args[0]);
                cheatChecker = true;
                System.out.println(numOfTAnk + " Tank in a field with " + args[1]);
            }
            game.play_game(numOfTAnk, cheatChecker);
        } catch (IndexOutOfBoundsException e){
            e.printStackTrace();
        }
    }
}
