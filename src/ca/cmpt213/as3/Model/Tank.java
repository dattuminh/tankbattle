package ca.cmpt213.as3.Model;
import java.util.ArrayList;

/**
 * the class hold information of tank
 * contain: damage and health
 * damage is store at arraylist so everytime the tank got hit,
 * the damage will be remove until it hit 0
 */
public class Tank {

    public Tank(){
        damage.add(20);
        damage.add(5);
        damage.add(2);
        damage.add(1);
        damage.add(0);
        health = HEALTH;
    }

    public int getHealth() {
        return health;
    }

    public int getDamage() {
        return damage.get(damage_current_tank);
    }

    public void tank_got_hit() {
        health--;
        damage.remove(damage_current_tank);
    }

    private ArrayList<Integer> damage = new ArrayList<Integer>();
    private int health;
    private int damage_current_tank = 0;
    private final int HEALTH = 4;
    //============================


}
