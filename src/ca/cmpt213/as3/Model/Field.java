package ca.cmpt213.as3.Model;
import java.util.ArrayList;

/**
 * the class generate random tank form and random tank position on the field
 * The field class is making the field of the game by MAX_FIELD as the maximum column and rows
 *
 */

public class Field {
    private static final int MAX_FIELD = 10;
    private static final int TANK_SIZE = 4;
    private int[][] tankField = new int[MAX_FIELD][MAX_FIELD];
    private String[][] userField = new String[MAX_FIELD][MAX_FIELD];
    private ArrayList<Integer> emptyCelsOfTanks = new ArrayList<>();


    public Field(){
        for (int i = 0; i < MAX_FIELD; i++) {
            for (int j = 0; j < MAX_FIELD; j++) {
                tankField[i][j] = -1;
                emptyCelsOfTanks.add(i * MAX_FIELD + j);
            }
        }
    }

    public Field(String user){
        for (int i = 0; i < MAX_FIELD; i++) {
            for (int j = 0; j < MAX_FIELD; j++) {
                userField[i][j] = user;
            }
        }
    }
    //incrasing listOfTanks and empty cellOfTanks-> final position deploying Tank
    // for loop for how much tank need to be deploy
    public void deployTank(int numOfTank){
        for(int i = 0; i < numOfTank; i++){
            randomizeTankPosition(i);
        }
    }
    public void randomizeTankPosition(int tankName) {
        int firstIndex = (int) (Math.random() * emptyCelsOfTanks.size());
        int indexField = emptyCelsOfTanks.get(firstIndex);
        ArrayList<Integer> possibleCells = new ArrayList<>(); // to insert 2 condition: occupied & algorithm to add all possibilities;
        ArrayList<Integer> tankLocation = new ArrayList<>();
        tankLocation.add(indexField);
//        System.out.println("indexField:"  + indexField);
//        System.out.println("tankLocation" + tankLocation);

        for (int i = 1; i < TANK_SIZE; i++) {
//            System.out.println("i:" + i);
            checkPossibleDirection(tankLocation.get(i - 1), possibleCells, tankLocation);
            System.out.println("size: " + possibleCells.size());
            int randomIndex = (int) (Math.random() * possibleCells.size());
            if(possibleCells.size() <= 0 ){
                throw new IndexOutOfBoundsException("");
            }
            int nextIndex = possibleCells.get(randomIndex);
            possibleCells.remove(randomIndex);
            tankLocation.add(nextIndex);
        }

        for(int t = 0; t < TANK_SIZE; t++){
            int column = tankLocation.get(t) % 10;
            int row = tankLocation.get(t) / 10;
            tankField[row][column] = tankName;
            emptyCelsOfTanks.remove(tankLocation.get(t));
        }
    }
    //take an input of initial position, take all the possible cells surrounded and update possible cells from randomizeTankPosition
    private void checkPossibleDirection(int index, ArrayList<Integer> possibleCells, ArrayList<Integer> tankLocator){
        int column = index % 10;
        int row = index / 10;
        System.out.println("tankLocation = " + tankLocator);

        if(column <= MAX_FIELD && row - 1 >= 0 ){
//            System.out.println("hello" + index);
            if(tankField[row - 1][column] == - 1 && !(tankLocator.contains((row-1)*MAX_FIELD+column))){
                possibleCells.add((row-1)*MAX_FIELD+column);
            }
        }
        else{
            System.out.println("ERROR: Index Out of Order");
        }
        if(column <= MAX_FIELD && row + 1 < MAX_FIELD){
//            System.out.println("hello" + index);
            if(tankField[row + 1][column] == - 1 && !(tankLocator.contains((row+1)*MAX_FIELD+column))){
                possibleCells.add((row+1)*MAX_FIELD+column);
            }
        }
        else{
//            System.out.println("ERROR: Index Out of Order");
        }
        if(column -1 >= 0 && row <= MAX_FIELD){
//            System.out.println("hello" + index);
            if(tankField[row][column -1] == - 1 && !(tankLocator.contains(row*MAX_FIELD+(column-1)))){
                possibleCells.add(row*MAX_FIELD+(column-1));
            }
        }
        else{
//            System.out.println("ERROR: Index Out of Order");
        }
        if(column + 1 < MAX_FIELD && row <= MAX_FIELD){
//            System.out.println("hello" + index);
            if(tankField[row][column + 1] == - 1 && !(tankLocator.contains(row*MAX_FIELD+(column+1)))){
                possibleCells.add(row*MAX_FIELD+(column+1));
            }
        }
        else{
//            System.out.println("ERROR: Index Out of Order");
        }
    }

    public String toStringTank() {
        String display = " ";
        for (int k = 0; k < MAX_FIELD + 1; k++){
            if (k == 0){
                display = display + "  ";
            }
            else {
                display = display + " " + k + " ";
            }
        }
        display = display + "\n";
        for (int i = 0; i < MAX_FIELD; i++){
            char onHoldFirstColumn = (char) (65 + i);
            display = display + " " + onHoldFirstColumn + " ";
            for (int j = 0; j < MAX_FIELD; j++) {
                if (tankField[i][j] == -1) {
                    display = display + " . ";
                }
                else if(tankField[i][j] > 96){
                    char onHoldForBeingHit = (char) tankField[i][j];
                    display = display + " " + onHoldForBeingHit +  " ";
                }
                else {
                    char onHold = (char) (tankField[i][j] + 65);
                    display = display + " " + onHold +  " ";
                }
            }
            display = display + "\n";
        }
        return display;
    }

    public String toStringUSer() {
        String display = " ";
        for (int k = 0; k < MAX_FIELD + 1; k++){
                if (k == 0){
                    display = display + "  ";
                }
            else {
                display = display + " " + k +
                        " ";
            }
        }
        display = display + "\n";

        for (int i = 0; i < MAX_FIELD; i++){
            char onHold = (char) (65 + i);
            display = display + " " + onHold + " ";

            for (int j = 0; j < MAX_FIELD; j++){
                display =  display + " " + userField[i][j] + " ";
            }
            display = display + "\n";
        }
        return display;
    }



    public void setPlayerFieldMiss(int row, int column){
        userField[row][column] = " ";
    }
    public void setPlayerFieldHit(int row, int column){
        userField[row][column] = "X";
    }
    public int checkHit( int row, int column){
        return tankField[row][column];
    }
    public void setTankField(int row, int column){
        tankField[row][column] = tankField[row][column] + 97;
    }
}
