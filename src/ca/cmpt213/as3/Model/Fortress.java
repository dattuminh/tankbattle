package ca.cmpt213.as3.Model;

/**
 * contain the information of player
 * contains: player health
 */
public class Fortress {
    public Fortress(){
        health = 1500;
        alive = true;
    }
    public int getHealth(){
        return this.health;
    }
    public void gotHit(int totalTankDamage){
        health = health - totalTankDamage;
        if(health < 0){
            health = 0;
            alive = false;
        }
    }
    public boolean isAlive(){
        return this.alive;
    }

    private int health;
    private boolean alive;

}
